package mysql

import (
	"context"
	"fmt"
	"license-api/service"
	"license-api/service/entity"
	"log"
	"os"
	"sync"
	"time"
	gormMySQL "gorm.io/driver/mysql"
	"github.com/go-sql-driver/mysql"
	"gorm.io/gorm"
)

var (
	once          sync.Once
	dbConnection  *gorm.DB
)

type mysqlRepo struct {
	db *gorm.DB
}

func ConnectDBWithRetry(retryAttempts int ) (*gorm.DB, error) {
	// Retry configuration
	retryDelay := time.Second * 5

	var connectErr error

	for attempt := 1; attempt <= retryAttempts; attempt++ {
		once.Do(func() {
			// Connect to MySQL using Gorm
			dsn := fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?charset=utf8mb4&parseTime=True&loc=Local", os.Getenv("MYSQL_USER"), os.Getenv("MYSQL_PASSWORD"), os.Getenv("MYSQL_HOST"), os.Getenv("MYSQL_DATABASE"))
			db, err := gorm.Open(gormMySQL.Open(dsn), &gorm.Config{})

			if err != nil {
				log.Printf("Failed to connect to DB (Attempt %d of %d): %v", attempt, retryAttempts, err)
				connectErr = err
				if attempt == retryAttempts {
					// log.Fatal("Failed to connect to DB after multiple attempts.")
					return
				}
				time.Sleep(retryDelay)
				return
			}

			fmt.Println("Connected to MySQL with Gorm!")

			dbConnection = db
		})

		// Use the mutex to safely check if the DB connection is available
		if dbConnection != nil {
			return dbConnection, nil
		}
	}
	dbConnection.AutoMigrate(&entity.License{})
	// This line should not be reached, but it's here to satisfy the function signature
	return nil, connectErr
}

func NewMySQLRepo(db *gorm.DB) service.LicenseRepository {
	return &mysqlRepo{
		db: db,
	}
}

func (repo mysqlRepo) InsertLicense(ctx context.Context, license *entity.License) error {
	if err := repo.db.Create(license).Error; err != nil {
		if mysqlErr, ok := err.(*mysql.MySQLError); ok {
			switch mysqlErr.Number {
			case 1062: // MySQL code for duplicate entry
				// Handle duplicate entry
				return entity.ErrExistCompany
			default:
				// Handle other errors
			}
		}
	}
	return nil
}