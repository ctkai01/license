package mongodb

import (
	"context"
	"fmt"
	"log"
	"os"
	"sync"
	"time"

	// "log"

	"github.com/patrickmn/go-cache"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	once            sync.Once
	mongoConnection *mongo.Client
)

type Company struct {
	ID        primitive.ObjectID `bson:"_id"`
	UpdateAt  time.Time          `bson:"update_at"`
	CreateAt  time.Time          `bson:"create_at"`
	IsDeleted bool               `bson:"is_deleted"`
	Name      string             `bson:"name"`
	OwnerID   primitive.ObjectID `bson:"owner_id"`
	V         int                `bson:"__v"`
}

type documentKey struct {
	ID primitive.ObjectID `bson:"_id"`
}
type DbEvent struct {
	DocumentKey   documentKey `bson:"documentKey"`
	OperationType string      `bson:"operationType"`
	FullDocument  bson.M      `bson:"fullDocument"`
}

func ListenToDBChangeStream(
	collection *mongo.Collection,
	cacheStore *cache.Cache,
) {
	routineCtx, cancelFn := context.WithCancel(context.Background())
	_ = cancelFn

	stream, err := collection.Watch(context.TODO(), mongo.Pipeline{})
	if err != nil {
		log.Fatalln(err)
	}
	// Cleanup defer functions when this function exits
	defer stream.Close(routineCtx)

	// Whenever there is a change in the bike-factory collection, decode the change
	for stream.Next(routineCtx) {
		var DbEvent DbEvent
		if err := stream.Decode(&DbEvent); err != nil {
			panic(err)
		}
		// Print out the document that was inserted
		if DbEvent.OperationType == "insert" {
			// const key = `company:${company._id}:owner`
			fullDocument, _ := DbEvent.FullDocument["fullDocument"].(map[string]interface{})

			id, idOK := fullDocument["_id"].(string)
			ownerID, ownerIDOK := fullDocument["owner_id"].(string)

			if idOK && ownerIDOK {
				key := fmt.Sprintf("company:%s:owner", id)
				cacheStore.Set(key, ownerID, cache.NoExpiration)
			}
		}
	}
}

func LoadAllOwnerCompany(collection *mongo.Collection, cacheStore *cache.Cache) {
	cur, err := collection.Find(context.TODO(), bson.D{{}}, options.Find())
	if err != nil {
		log.Fatal(err)
	}

	for cur.Next(context.TODO()) {
		//Create a value into which the single document can be decoded
		var company Company
		err := cur.Decode(&company)
		if err != nil {
			log.Fatal(err)
		}
		key := fmt.Sprintf("company:%s:owner", company.ID.String())
		cacheStore.Set(key, company.OwnerID, cache.NoExpiration)
	}
}

func ConnectMongoWithRetry(retryAttempts int) (*mongo.Client, error) {
	// Retry configuration
	retryDelay := time.Second * 5

	var connectErr error

	for attempt := 1; attempt <= retryAttempts; attempt++ {
		once.Do(func() {
			// Connect to MySQL using Gorm

			client, err := mongo.Connect(
				context.TODO(),
				options.Client().ApplyURI(os.Getenv("MONGODB_URI")),
			)
			if err != nil {
				log.Printf("Failed to connect to DB (Attempt %d of %d): %v", attempt, retryAttempts, err)
				connectErr = err
				if attempt == retryAttempts {
					// log.Fatal("Failed to connect to DB after multiple attempts.")
					return
				}
				time.Sleep(retryDelay)
				return
			}

			fmt.Println("Connected to Mongo with Gorm!")

			mongoConnection = client
		})

		// Use the mutex to safely check if the DB connection is available
		if mongoConnection != nil {
			return mongoConnection, nil
		}
	}

	// This line should not be reached, but it's here to satisfy the function signature
	return nil, connectErr
}
