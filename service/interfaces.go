package service

import (
	"context"
	"license-api/service/entity"
)

type LicenseUseCase interface {
	RegisterLicense(ctx context.Context, req * entity.RegisterLicenseReq) (*entity.License, error)
	VerifyLicense(ctx context.Context, req * entity.VerifyLicenseReq) (*entity.LicensePayload, error)
}

type LicenseRepository interface {
	InsertLicense(ctx context.Context, license *entity.License) error
}