package entity

import (
	"errors"
	"time"
)

var (
	ErrExistCompany = errors.New("this company has been registered license")
	ErrVerifyLicenseFailed = errors.New("verify license failed")
)

type License struct {
	ID        int        `json:"id" gorm:"primary_key"`
	CompanyID string     `json:"company_id" gorm:"not null;unique"`
	Token     string     `json:"token" gorm:"not null"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

type RegisterLicenseReq struct {
	Organization    string `form:"organization" validate:"required,lte=255"`
	GoogleAppID     string `form:"google_app_id" validate:"required"`
	GoogleAppSecret string `form:"google_app_secret" validate:"required"`
	Host            string `form:"host" validate:"required"`
	CompanyID        string
}

type VerifyLicenseReq struct {
	License string `form:"license" validate:"required"`
}

type LicensePayload struct {
	Organization    string `json:"organization"`
	GoogleAppID     string `json:"google_app_id"`
	GoogleAppSecret string `json:"google_app_secret"`
	Host            string `json:"host"`
}
