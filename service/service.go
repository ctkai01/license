package service

import (
	"context"
	"license-api/service/entity"
	"license-api/service/utils"

	"github.com/patrickmn/go-cache"
	// "license-api/service/"
)

type service struct {
	licenseRepository LicenseRepository
	cacheStore *cache.Cache
}

func NewService(repository LicenseRepository, cacheStore *cache.Cache) *service {
	return &service{
		licenseRepository: repository,
		cacheStore: cacheStore,
	}
}

func (s service) RegisterLicense(ctx context.Context, req * entity.RegisterLicenseReq) (*entity.License, error) {
	licensePayload := &entity.LicensePayload{
		Organization:    req.Organization,
		GoogleAppID:     req.GoogleAppID,
		GoogleAppSecret: req.GoogleAppSecret,
		Host:            req.Host,
	}
	
	licenseStr, err := utils.GenerateNewLicense(licensePayload)
	
	if err != nil {
		return nil, err
	}

	newLicense := &entity.License{
		CompanyID: req.CompanyID,
		Token:     licenseStr,
	}
	
	if err := s.licenseRepository.InsertLicense(ctx, newLicense); err != nil {
		return nil, err
	}
	
	return newLicense, nil
}

func (s service) VerifyLicense(ctx context.Context, req * entity.VerifyLicenseReq) (*entity.LicensePayload, error) {

	result, err := utils.VerifyLicense(req.License)
	
	if err != nil {
		return nil, err
	}

	return result, nil
}