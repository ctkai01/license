package utils

import (
	"crypto/x509"
	_ "embed"
	"encoding/pem"
	"fmt"
	"license-api/service/entity"
	"time"

	"github.com/dgrijalva/jwt-go"
)

//go:embed private-key.pem
var privateKey []byte
//30 days
const DurationPeriod = time.Hour * 24 * 30

func GenerateNewLicense(payload *entity.LicensePayload) (string, error) {

	// Parse the PEM-encoded private key
	block, _ := pem.Decode(privateKey)
	if block == nil {
		return "", fmt.Errorf("failed to parse PEM block containing the private key")
	}

	key, err := x509.ParseECPrivateKey(block.Bytes)
	if err != nil {
		return "", fmt.Errorf("error parse JWT: %v", err)

	}

	claims := jwt.MapClaims{
		"organization": payload.Organization,
		"exp": time.Now().Add(DurationPeriod).Unix(),
		"host": payload.Host,
		"google_app_id": payload.GoogleAppID,
		"google_app_secret": payload.GoogleAppSecret,
		"iat":  time.Now().Unix(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodES256, claims)

	signedToken, err := token.SignedString(key)
	if err != nil {
		return "", fmt.Errorf("error signing JWT: %v", err)

	}
	return signedToken, nil
}
