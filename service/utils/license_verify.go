package utils

import (
	"crypto/ecdsa"
	"crypto/x509"
	_ "embed"
	"encoding/pem"
	"fmt"
	"license-api/service/entity"

	"github.com/mitchellh/mapstructure"

	// "self-license-api/app/models"

	"github.com/dgrijalva/jwt-go"
	// "github.com/dgrijalva/jwt-go"
)

//go:embed public-key.pem
var publicKey []byte

func VerifyLicense(license string) (*entity.LicensePayload, error) {

	// Parse the PEM-encoded private key
	block, _ := pem.Decode(publicKey)
	if block == nil {
		return nil, nil
	}
	key, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, fmt.Errorf("error parse: %v", err)

	}

	ecdsaKey, ok := key.(*ecdsa.PublicKey)
	if !ok {
		return nil, fmt.Errorf("parsed key is not an ECDSA public key")
	}

	// Verify the JWT signature
	token, err := jwt.Parse(license, func(token *jwt.Token) (interface{}, error) {
		// Ensure the signing method is ES256
		if _, ok := token.Method.(*jwt.SigningMethodECDSA); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}

		// Return the public key for verification
		return ecdsaKey, nil
	})

	if err != nil {
		return nil, fmt.Errorf("license parsing failed: %v", err)
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		// Convert claims to LicenseClaims struct
		payload := &entity.LicensePayload{}

		err := mapstructure.Decode(claims, payload)

		if err != nil {
			return nil, fmt.Errorf("error decoding claims: %v", err)
		}

		// if payload.Exp < float64(time.Now().Unix()) {
		// 	return nil, fmt.Errorf("license had expired")
		// }

		return payload, nil
	}

	return nil, fmt.Errorf("license is not valid")
}
