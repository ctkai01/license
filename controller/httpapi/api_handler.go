package httpapi

import (
	"fmt"
	"license-api/service"
	"license-api/service/entity"
	"license-api/utils"
	"license-api/middlewares"

	"github.com/go-playground/validator/v10"

	"github.com/gofiber/fiber/v2"
)

type apiController struct {
	licenseService service.LicenseUseCase
}

func NewAPIController(s service.LicenseUseCase) *apiController {
	return &apiController{
		licenseService: s,
	}
}

func (api apiController) registerLicense() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		licenseReq := &entity.RegisterLicenseReq{}

		if err := c.BodyParser(licenseReq); err != nil {
			// Return status 400 and error message.
			return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
				"error": true,
				"msg":  err.Error(),
			})
		}
		fmt.Println("licenseReq: ", licenseReq)
		validate := validator.New()

		if err := validate.Struct(licenseReq); err != nil {
			// Return, if some fields are not valid.
			return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
				"error": true,
				"msg":   utils.ValidatorErrors(err),
			})
		}
		companyID, _ := c.Locals("company").(string)
		licenseReq.CompanyID = companyID
		result, err := api.licenseService.RegisterLicense(c.Context(), licenseReq)

		if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"error": true,
				"msg":   err.Error(),
			})
		}

		// Return status 200 OK.
		return c.JSON(fiber.Map{
			"error": false,
			"msg":   "Create license successfully!",
			"res": map[string]interface{}{
				"license": result,
			},
		})
	}
}

func (api apiController) verifyLicense() func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		verifyLicenseReq := &entity.VerifyLicenseReq{}

		if err := c.BodyParser(verifyLicenseReq); err != nil {
			// Return status 400 and error message.
			return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
				"error": true,
				"msg1":  err.Error(),
			})
		}

		validate := validator.New()

		if err := validate.Struct(verifyLicenseReq); err != nil {
			// Return, if some fields are not valid.
			return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
				"error": true,
				"msg":   utils.ValidatorErrors(err),
			})
		}

		result, err := api.licenseService.VerifyLicense(c.Context(), verifyLicenseReq)

		if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
				"error": true,
				"msg":   entity.ErrVerifyLicenseFailed.Error(),
			})
		}

		// Return status 200 OK.
		return c.JSON(fiber.Map{
			"error": false,
			"msg":   "Verify license successfully!",
			"res": map[string]interface{}{
				"license": result,
			},
		})
	}
}
func (api apiController) SetupRoute(group fiber.Router) {
	group.Post("/license", middlewares.IsAdmin, api.registerLicense())
	group.Post("/license/verify", api.verifyLicense())
}
