package main

import (
	"context"
	"fmt"
	"license-api/controller/httpapi"
	"license-api/infras/mongodb"
	"license-api/infras/mysql"
	"license-api/middlewares"
	"license-api/service"
	"license-api/utils"
	"log"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/patrickmn/go-cache"
)


func main() {
	app := fiber.New()
	cacheStore := cache.New(cache.NoExpiration, cache.NoExpiration)
	//Connect Mysql
	db, err := mysql.ConnectDBWithRetry(3)

	if err != nil {
		log.Fatalln(err)
	}
	
	//Connect Mongo
	dbMongo, err := mongodb.ConnectMongoWithRetry(3)

	if err != nil {
		log.Fatalln(err)
	}

	defer dbMongo.Disconnect(context.TODO())

	database := dbMongo.Database("tracking")
	collection := database.Collection("companies")

	
	app.Use(middlewares.CacheMiddleware(cacheStore, dbMongo))

	go mongodb.ListenToDBChangeStream(collection, cacheStore)
	go mongodb.LoadAllOwnerCompany(collection, cacheStore)

	licenseRepository := mysql.NewMySQLRepo(db)

	licenseService := service.NewService(licenseRepository, cacheStore)

	controller := httpapi.NewAPIController(licenseService)

	groupAPI := app.Group(fmt.Sprintf("/api/%s", os.Getenv("VERSION")))

	controller.SetupRoute(groupAPI)

	utils.StartServerWithGracefulShutdown(app)

}
