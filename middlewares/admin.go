package middlewares

import (
	"context"
	"fmt"

	"github.com/gofiber/fiber/v2"
	"github.com/patrickmn/go-cache"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	// "license-api"
)

type UserLogin struct {
	ID    primitive.ObjectID `bson:"_id"`
	Email string             `bson:"email"`
	Token string             `bson:"token"`
	User  primitive.ObjectID `bson:"user,omitempty"`
}

type User struct {
	ID      primitive.ObjectID `bson:"_id,omitempty"`
	Company primitive.ObjectID `bson:"company,omitempty"`
	Email   string             `bson:"email,omitempty"`
}

func IsAdmin(c *fiber.Ctx) error {
	token := ""
	tokenReq := c.Cookies("tracking-key")

	if tokenReq == "" {
		tokenReq = c.Get("tracking-key")
	}
	token = tokenReq

	//Get user
	cacheData, _ := c.Locals("cache").(*cache.Cache)
	mongoClient, _ := c.Locals("mongoClient").(*mongo.Client)

	var result UserLogin
	err := mongoClient.Database("tracking").Collection("user_login").FindOne(context.Background(), bson.D{
		{
			Key:   "token",
			Value: token,
		},
	}).Decode(&result)

	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"error": true,
			"msg":   "Token is invalid",
		})
	}

	var user User

	err = mongoClient.Database("tracking").Collection("users").FindOne(context.Background(), bson.D{
		{
			Key:   "_id",
			Value: result.User,
		},
	}).Decode(&user)

	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"error": true,
			"msg":   "Token is invalid",
		})
	}

	ownerId, found := cacheData.Get(fmt.Sprintf(`company:%s:owner`, user.Company))

	if found {
		if user.ID != ownerId {
			return c.Status(fiber.StatusForbidden).JSON(fiber.Map{
				"error": true,
				"msg":   "Permission denied",
			})
		}
	} else {
		return c.Status(fiber.StatusForbidden).JSON(fiber.Map{
			"error": true,
			"msg":   "Permission denied",
		})
	}
	c.Locals("company", user.Company.Hex())
	return c.Next()
}
