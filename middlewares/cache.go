package middlewares

import (
	"github.com/gofiber/fiber/v2"
	"github.com/patrickmn/go-cache"
	"go.mongodb.org/mongo-driver/mongo"
)

func CacheMiddleware(cache *cache.Cache, mongoClient *mongo.Client) fiber.Handler {
	return func(c *fiber.Ctx) error {
		// Add the cache to the context
		c.Locals("cache", cache)
		c.Locals("mongoClient", mongoClient)

		// Continue to the next handler
		return c.Next()
	}
}